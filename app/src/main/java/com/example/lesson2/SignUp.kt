package com.example.lesson2

import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class SignUp : AppCompatActivity() {
    lateinit var sPref: SharedPreferences
    val APP_PREFERENCES = "settings"
    private val SAVED_PASS: String = "saved_pass"
    private val CONFIRM_PASS: String = "confirm_pass"
    private val CITY: String = "CITY"
    private val FULL_NAME: String = "full_name"
    private val SAVED_EMAIL: String = "saved_email"
    lateinit var list: List<String>
    lateinit var createPass: EditText
    lateinit var confirmPass: EditText
    lateinit var city: EditText
    lateinit var fullName: EditText
    lateinit var email: EditText
    lateinit var ed: Editor


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up)
        list = listOf(SAVED_PASS, CONFIRM_PASS, CITY, FULL_NAME, SAVED_EMAIL)
        createPass = findViewById<EditText>(R.id.create_pass)
        confirmPass = findViewById<EditText>(R.id.confirm_pass)
        city = findViewById<EditText>(R.id.city)
        fullName = findViewById<EditText>(R.id.full_name)
        email = findViewById<EditText>(R.id.email)
        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE)
        ed = sPref.edit()
        findViewById<ImageButton>(R.id.sign_up).setOnClickListener {
            saveText()
            if (sPref.getString(SAVED_PASS,"") !="" && sPref.getString(CONFIRM_PASS,"")
                    ==sPref.getString(SAVED_PASS,"") &&
                            sPref.getString(CITY,"")!="" && sPref.getString(FULL_NAME,"")!="" &&
                            sPref.getString(SAVED_EMAIL,"")!="" ) {
                startActivity(Intent(this, SignIn::class.java))
            } else {
                Toast.makeText(this, "field is not filled in or entered incorrectly", Toast.LENGTH_SHORT).show()
            }


        }

    }

    fun saveText() {
        ed.putString(SAVED_PASS, createPass.text.toString())
        ed.putString(CITY, city.text.toString())
        ed.putString(CONFIRM_PASS, confirmPass.text.toString())
        ed.putString(FULL_NAME, fullName.text.toString())
        ed.putString(SAVED_EMAIL, email.text.toString())
        ed.apply()
    }

}




//if () {
//    Toast.makeText(this, "Confirm password!", Toast.LENGTH_SHORT).show()
//    if () {
//        Toast.makeText(this, "Enter city!", Toast.LENGTH_SHORT).show()
//        if () {
//            Toast.makeText(this, "Enter full name!", Toast.LENGTH_SHORT).show()
//            if () {
//                Toast.makeText(this, "Enter email!", Toast.LENGTH_SHORT).show()
//            }