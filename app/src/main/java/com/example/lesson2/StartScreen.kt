package com.example.lesson2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class StartScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_screen)

        findViewById<ImageButton>(R.id.sign_in_start_screen).setOnClickListener {
            startActivity(Intent(this, SignIn::class.java))
        }
        findViewById<ImageButton>(R.id.sign_up_start_screen).setOnClickListener {
            startActivity(Intent(this, SignUp::class.java))
        }


    }

}