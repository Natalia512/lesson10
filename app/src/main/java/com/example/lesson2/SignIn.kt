package com.example.lesson2

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class SignIn : AppCompatActivity() {
    val APP_PREFERENCES = "settings"
    lateinit var sPref: SharedPreferences

    private val SAVED_PASS: String = "saved_pass"
    private val SAVED_EMAIL: String = "saved_email"
    lateinit var pass: EditText
    lateinit var email: EditText
    lateinit var savedPass: String
    lateinit var savedEmail: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_in)

        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE)
        savedPass = sPref.getString(SAVED_PASS, "").toString()
        savedEmail = sPref.getString(SAVED_EMAIL, "").toString()
        pass = findViewById(R.id.pass)
        email = findViewById(R.id.email)

        findViewById<ImageButton>(R.id.sign_in).setOnClickListener {
            if (savedPass == pass.text.toString() && savedEmail == email.text.toString()) {
                // pass.setText(savedText)
                startActivity(Intent(this, HistoryList::class.java))
            } else {
                Toast.makeText(this,"Incorrect login or password", Toast.LENGTH_SHORT).show()

            }

        }
    }
}
