package com.example.lesson2

import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView

class AdapterHistory(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class ViewHolderHistory(view: View) : RecyclerView.ViewHolder(view) {
        val titleHistory: TextView
        val textHistory: TextView
        val star1: ImageView
        val star2: ImageView
        val star3: ImageView
        val star4: ImageView
        val star5: ImageView

        init {
            titleHistory = view.findViewById(R.id.title_history)
            textHistory = view.findViewById(R.id.history)

            star1 = view.findViewById(R.id.star1)
            star2 = view.findViewById(R.id.star2)
            star3 = view.findViewById(R.id.star3)
            star4 = view.findViewById(R.id.star4)
            star5 = view.findViewById(R.id.star5)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ViewHolderHistory(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            context.startActivity(DetailsHistory().getIntent(context))
        }
    }

    override fun getItemCount() = 10

}