package com.example.lesson2

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class DetailsHistory : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details_history)

        findViewById<ImageButton>(R.id.add_fav).setOnClickListener {
            Toast.makeText(this,"History was added to favourites",Toast.LENGTH_SHORT).show()
        }
    }

    fun getIntent(context: Context): Intent {
        return Intent(context, DetailsHistory::class.java)
    }
}